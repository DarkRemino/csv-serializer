# csv-serializer
### Made by Orlin Kralov



This is a C program designed to take in either a csv file and serialize it to .bin,
or the opposite, take a .bin file and  deserialize it to a.csv one.

the program takes 3 CLI inputs:
* -s specifies whether the program should be run with or without logs
* -f provides a file path to the file to be serialized/deserialized
* -c this option should be either "serialize" or "deserialize", telling the program what to do

After receiving the inputs, the program runs several validations to 
make sure all the arguments are correct, and then does the action specified.

#### block scheme of logical tree  of the program:
![alt text](        if(strcmp(cli_input_c, "serialize")==0)
{
if(cli_input_s==0)
{
printf("The program has been set to serialize the data of a .csv file.\n");
}
if(strcmp(ext,".bin")==0)
{
if (cli_input_s==0)
{
printf("You've set the program to serialize a .bin file (which is impossible), aborting.");
}
abort();
}
})
