#include <stdio.h>
#include <stdlib.h>
#include <string.h>//strcmp()
#include <unistd.h>//getopt()

//global utility vars for defining the size of the struct and array of structs
#define MAX_LINE_LENGTH 4096
#define MAX_RECORDS 10000
#define MAX_ORDER_ID_LENGTH 14
#define MAX_ORDER_DATE_LENGTH 15
#define MAX_CUSTOMER_ID_LENGTH 15
#define MAX_CITY_LENGTH 20

#define MAX_PRODUCT_ID_LENGTH 20


char region[5][15] =
{
    "Central",
    "West",
    "South",
    "East",
    "North"
};

char state[50][30] =
{
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming"
};

char category[3][15] =
{
    "Furniture",
    "Office Supplies",
    "Technology"
};

char sub_category[][15] =
{
    "Accessories",
    "Appliances",
    "Art",
    "Binders",
    "Bookcases",
    "Chairs",
    "Furnishings",
    "Labels",
    "Machines",
    "Paper",
    "Phones",
    "Storage",
    "Supplies",
    "Tables"
};

void serialize(FILE* input_file, FILE* output_file);
void deserialize(FILE* input_file, FILE* output_file);

//structure of a single row within the CSV
struct Order
{
    int row_id;
    char order_id[MAX_ORDER_ID_LENGTH];
    char order_date[MAX_ORDER_DATE_LENGTH];
    char customer_id[MAX_CUSTOMER_ID_LENGTH];
    char city[MAX_CITY_LENGTH];
    int state;
    int postal_code;
    int region;
    char product_id[MAX_PRODUCT_ID_LENGTH];
    int category;
    int sub_category;
    float price;
};

//array of structs to be written and read from
struct Order orders[MAX_RECORDS];
int num_records = 0;

int main(int argc, char** argv)
{
    int opt;
    int cli_input_s = 0;
    char* cli_input_f = NULL;
    char *cli_input_c = NULL;
    char* ext;

    //utility file pointers for use in the serialize and deserialize functions
    FILE* input_file;
    FILE* output_file;

    while ((opt = getopt(argc, argv, "sf:c:")) != -1)
    {
        switch (opt)
        {
            case 's':
                printf("Program is executing without logs.\n");
                cli_input_s++;
                break;
            case 'f':
                cli_input_f = optarg;
                break;
            case 'c':
                cli_input_c = optarg;
                break;
            case '?':
                printf("The program has encountered an unknown argument, aborting.\n");
                abort();
            default:
                printf("I don't know what exactly is going on but...");
                break;
        }
    }

    ext = strrchr(cli_input_f,'.');

    if(fopen(cli_input_f,"r")==NULL)
    {
        if(cli_input_s==1) abort();
        else
        {
            printf("Something went wrong when trying to read from the file, aborting.");
            abort();
        }
    }
    else
    {
        //check file extension, if correct will continue, if not it aborts
        if (strcmp(ext, ".bin")==0)
        {
            if (cli_input_s==0)
            {
                printf("The program has accepted a path to a .bin file.\n");
            }
            input_file = fopen(cli_input_f,"rb");
            output_file = fopen("generated_sales.csv", "w");
        }
        else if (strcmp(ext, ".csv")==0)
        {
            if (cli_input_s==0)
            {
                printf("The program has accepted a path to a .csv file.\n");
            }
            input_file = fopen(cli_input_f,"r");
            output_file = fopen("generated_sales.bin", "wb");
        }
        else
        {
            if(cli_input_s==0)
            {
                printf("The specified file path is valid, but leads to a wrong file format, aborting.");
            }
            abort();
        }

        //check -c input and start the appropriate function if the file extension provided is correct(will not allow invalid actions)
        if(strcmp(cli_input_c, "serialize")==0)
        {
            if(cli_input_s==0)
            {
                printf("The program has been set to serialize the data of a .csv file.\n");
            }
            if(strcmp(ext,".bin")==0)
            {
                if (cli_input_s==0)
                {
                    printf("You've set the program to serialize a .bin file (which is impossible), aborting.");
                }
                abort();
            }
            serialize(input_file,output_file);
        }
        else if(strcmp(cli_input_c, "deserialize")==0)
        {
            if(cli_input_s==0)
            {
                printf("The program has been set to deserialize the data of a .bin file.\n");
            }
            if(strcmp(ext,".csv")==0)
            {
                if (cli_input_s==0)
                {
                    printf("You've set the program to deserialize a .csv file (which is impossible), aborting.");
                }
                abort();
            }
            deserialize(input_file, output_file);
        }
        else
        {
            if(cli_input_s==0)
            {
                printf("Wrong argument for the -c option, aborting.");
            }
            abort();
        }
    }

    fclose(input_file);
    fclose(output_file);

    return 0;
}

//reads the CSV line by line, tokenizes using the "," as a delimiter, and assigns the values read to the corresponding struct element
//taking up the current [num_records] position in the array. After that it writes all valid elements (up to num_records) in the output file.
void serialize(FILE* input_file,FILE* output_file)
{
    char line[MAX_LINE_LENGTH];//utility variable to store a single line from the CSV input

    while(fgets(line, sizeof(line), input_file) != NULL)
    {
        char* token = NULL;

        token = strtok(line, ",");
        orders[num_records].row_id = atoi(token);

        token = strtok(NULL, ",");
        strncpy(orders[num_records].order_id, token, MAX_ORDER_ID_LENGTH);

        token = strtok(NULL, ",");
        strncpy(orders[num_records].order_date, token, MAX_ORDER_DATE_LENGTH);

        token = strtok(NULL, ",");
        strncpy(orders[num_records].customer_id, token, MAX_CUSTOMER_ID_LENGTH);

        token = strtok(NULL, ",");
        strncpy(orders[num_records].city, token, MAX_CITY_LENGTH);

        token = strtok(NULL, ",");
        for (int i = 0; i < 49; ++i)
        {
            if(strcmp(token,state[i])==0)
            {
                orders[num_records].state = i;
            }
        }

        token = strtok(NULL, ",");
        orders[num_records].postal_code = atoi(token);

        token = strtok(NULL, ",");
        for (int i = 0; i < 4; ++i)
        {
            if(strcmp(token,region[i])==0)
            {
                orders[num_records].region = i;
            }
        }

        token = strtok(NULL, ",");
        strncpy(orders[num_records].product_id, token, MAX_PRODUCT_ID_LENGTH);

        token = strtok(NULL, ",");
        for (int i = 0; i < 12; ++i)
        {
            if(strcmp(token,sub_category[i])==0)
            {
                orders[num_records].sub_category = i;
            }
        }
//        strncpy(orders[num_records].category, token, MAX_CATEGORY_LENGTH);

        token = strtok(NULL, ",");
        for (int i = 0; i < 2; ++i)
        {
            if(strcmp(token,category[i])==0)
            {
                orders[num_records].category = i;
            }
        }
//        strncpy(orders[num_records].sub_category, token, MAX_SUB_CATEGORY_LENGTH);

        token = strtok(NULL, ",");
        orders[num_records].price = atof(token);

        num_records++;
    }


    fwrite(&num_records, sizeof(num_records), 1, output_file);
    fwrite(orders, sizeof(struct Order), num_records, output_file);
    fclose(input_file);
    fclose(output_file);

}

void deserialize(FILE* input_file, FILE* output_file)
{
    //reads the number of valid elements (num_records) and then reads that many elements from the input, storing them in the struct array elements.
    //After that it cycles through the data read num_records number of times and writes it in the new file using the specified format.
    fread(&num_records, sizeof(num_records), 1, input_file);
    fread(orders, sizeof(struct Order), num_records, input_file);

    for (int i = 0; i < num_records; i++)
    {
        fprintf(output_file, "%d,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%.2f\n",
                orders[i].row_id,
                orders[i].order_id,
                orders[i].order_date,
                orders[i].customer_id,
                orders[i].city,
                state[orders[i].state],
                orders[i].postal_code,
                region[orders[i].region],
                orders[i].product_id,
                category[orders[i].category],
                sub_category[orders[i].sub_category],
                orders[i].price);
    }

    fclose(input_file);
    fclose(output_file);
}